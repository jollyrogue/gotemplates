package main

import (
	"html/template"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type Service struct {
	Service string `form:"entryService"`
	Date    string `form:"entryDate"`
	Cost    string `form:"entryCost"`
	Hours   string `form:"entryHours"`
	Total   float64
}

type Form struct {
	NameFirst   string `form:"nameFirst"`
	NameLast    string `form:"nameLast"`
	PhoneNumber string `form:"phoneNumber"`
	Email       string `form:"email"`
	Date        string `form:"dateEntry"`
	Message     string `form:"comments"`
	Table       []Service
	Total       float64
}

type PageIndex struct {
	Title    string
	Greeting string
}

var tIndex *template.Template
var tResponse *template.Template
var tDataIndex PageIndex

func main() {
	var err error

	tIndex, err = template.New("hello.tmpl").ParseFiles("templates/hello.tmpl")
	if err != nil {
		log.Fatalf("ERROR: 1 (main) Problem with template. %s", err)
	}

	tResponse, err = template.New("response.tmpl").ParseFiles("templates/response.tmpl")
	if err != nil {
		log.Fatalf("ERROR: 1 (main) Problem with template. %s", err)
	}

	tDataIndex.Title = "Index"
	tDataIndex.Greeting = "Welcome!"

	r := mux.NewRouter()
	r.HandleFunc("/", hello).Methods("GET")
	r.HandleFunc("/form", formCapture).Methods("POST").Headers("Content-Type", "application/x-www-form-urlencoded")
	log.Printf("Starting webserver...")
	log.Fatal(http.ListenAndServe(":8000", r))
}

func hello(w http.ResponseWriter, r *http.Request) {
	if err := tIndex.Execute(w, tDataIndex); err != nil {
		log.Printf("ERROR: 2 (hello) Problem with template. %s", err)
		http.Error(
			w,
			http.StatusText(http.StatusInternalServerError),
			http.StatusInternalServerError,
		)
		return
	}
}

func formCapture(w http.ResponseWriter, r *http.Request) {
	var err error
	var cost float64
	var hours float64

	body := Form{
		Total: 0.00,
	}

	log.Println("Starting form capture...")
	if err = r.ParseForm(); err != nil {
		log.Printf("ERROR: 3 (formCapture) Problem parsing form. %s", err)
		http.Error(
			w,
			http.StatusText(http.StatusInternalServerError),
			http.StatusInternalServerError,
		)
		return
	}
	log.Printf("Form: %+v", r.Form)

	body.NameFirst = r.PostFormValue("nameFirst")
	body.NameLast = r.PostFormValue("nameLast")
	body.PhoneNumber = r.PostFormValue("phoneNumber")
	body.Email = r.PostFormValue("email")
	body.Date = r.PostFormValue("dateEntry")
	body.Message = r.PostFormValue("comments")

	serviceNumber := len(r.PostForm["entryService"])
	for i := 0; i < serviceNumber; i++ {
		body.Table = append(
			body.Table,
			Service{
				Service: r.PostForm["entryService"][i],
				Date:    r.PostForm["entryDate"][i],
				Cost:    r.PostForm["entryCost"][i],
				Hours:   r.PostForm["entryHours"][i],
			},
		)

		cost, err = strconv.ParseFloat(r.PostForm["entryCost"][i], 64)
		if err != nil {
			log.Printf("ERROR: 300 (formCapture) Error parsing entryCost to float. %s", err)
			cost = 0.0
		}

		hours, err = strconv.ParseFloat(r.PostForm["entryHours"][i], 64)
		if err != nil {
			log.Printf("ERROR: 301 (formCapture) Error parsing entryHours to float. %s", err)
			hours = 0.0
		}

		body.Table[i].Total = cost * hours
		body.Total = body.Total + body.Table[i].Total
	}

	if err := tResponse.Execute(w, body); err != nil {
		log.Printf("ERROR: 302 (formCapture) Problem with template. %s", err)
		http.Error(
			w,
			http.StatusText(http.StatusInternalServerError),
			http.StatusInternalServerError,
		)
		return
	}
}
